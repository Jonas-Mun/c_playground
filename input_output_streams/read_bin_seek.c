#include <stdio.h>

struct rec {
	int x, y, z;
};

int main() 
{
	int counter;
	FILE *fptr;
	struct rec my_record;

	fptr = fopen("test.bin", "rb");
	if (!fptr) {
		printf("Unable to open file\n");
		return -1;
	}

	for (counter = 9; counter >= 0; counter--) {
		fseek(fptr,sizeof(struct rec) * counter, SEEK_SET);
		fread(&my_record, sizeof(struct rec), 1, fptr);
		printf("%x\n", my_record.x);
	}
	fclose(fptr);
	return 0;
}
