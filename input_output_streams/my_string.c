#include <string.h>

void reverse_string(char **buf) {
	int length = strlen(*buf);
	int mid_point = length / 2;

	char tmp_char;
	for (int i = 0; i < mid_point; i++) {
		tmp_char = (*buf)[i];
		(*buf)[i] = (*buf)[(length-1)-i];

		(*buf)[(length-1)-i] = tmp_char;
	}
}
