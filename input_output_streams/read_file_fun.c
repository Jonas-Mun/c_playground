#include <stdio.h>
#include <stdlib.h>

#define MAXBUFFER 512

int main(void) 
{
	FILE *fptr;

	fptr = fopen("text.txt", "r");

	size_t buf_size = MAXBUFFER;
	char *buffer = malloc(sizeof(char) * buf_size);

	ssize_t read = getline(&buffer, &buf_size, fptr);
	while(read != -1) {
		printf("%s", buffer);
		read = getline(&buffer, &buf_size, fptr);

	}

	fclose(fptr);
}
