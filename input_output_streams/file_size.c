#include <stdio.h>

int main()
{
	FILE *fptr;

	fptr = fopen("text.txt", "r");

	fseek(fptr,0, SEEK_END);
	int sz = ftell(fptr);

	rewind(fptr);

	int size = 0;
	while(getc(fptr) != EOF) {
		size++;
	}

	fclose(fptr);
	printf("%d\n", sz);
	printf("%d\n", size);
}
