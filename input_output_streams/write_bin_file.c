#include <stdio.h>

struct rec {
	int x, y, z;
};

int main()
{
	int counter;
	FILE *fptr;
	struct rec my_record;

	fptr = fopen("test.bin", "wb");
	if (!fptr) {
		printf("Unable to open file!\n");
		return -1;
	}

	for (counter = 1; counter <= 10; counter++) {
		my_record.x = counter;
		fwrite(&my_record, sizeof(struct rec), 1, fptr);
	}
	fclose(fptr);
	return 0;
}
