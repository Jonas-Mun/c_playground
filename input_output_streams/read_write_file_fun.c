#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "my_string.h"

void modify_line(char *buff);

int main(void) 
{
	// filestreams
	// -----------
	FILE *fptr_input;
	FILE *fptr_output;

	fptr_input = fopen("text.txt", "r");
	fptr_output = fopen("mod_text.txt", "w");

	size_t buf_size = 512;
	char *buffer = malloc(sizeof(char) * buf_size);

	// read & write
	// ------------
	ssize_t read = getline(&buffer, &buf_size, fptr_input);
	while(read != -1) {
		// manipulate string
		// -----------------
		//modify_line(buffer);
		reverse_string(&buffer);

		// write to file
		// -------------
		fprintf(fptr_output, "%s", buffer);

		read = getline(&buffer, &buf_size, fptr_input);
	}
}

void modify_line(char *buff) {
	int length = strlen(buff);

	for(int i = 0; i < length; i++) {
		// germify 'the'
		// -------------
		if (buff[i] == 't' && i <= length - 3 ) {
			if (buff[i+1] == 'h') {
				if (buff[i+2] == 'e') {
						buff[i] = 'z';
						buff[i+1] = 'e';
						buff[i+2] = 'e';
				}
			}
		}
	}
}
