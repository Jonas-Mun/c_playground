#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define START_TOTAL_STRINGS 10

int main()
{
	// init storage
	// ------------
	int total_string = START_TOTAL_STRINGS;
	char **dyn_strings = (char **) malloc(sizeof(char *) * total_string);

	if (dyn_strings == NULL) {
		printf("Unable to allocate memory\n");
		exit(1);
	}
		

	// init file stream
	// ----------------
	FILE *fptr;
	fptr = fopen("word.txt", "r");

	if (fptr == NULL) {
		printf("Unable to open file\n");
		exit(1);
	}

	char buffer[256];
	int str_len;	//used for allocating memory to string

	int current_string = 0;

	int max_incr = 1;
	int cur_incr = 0;
	while (fscanf(fptr, "%s", buffer) != EOF) {
		str_len = strlen(buffer);
		//printf("%s ", buffer);

		// realloc strings table
		// ---------------------
		if (current_string == total_string) {
			if (total_string == 20) {
				printf("Reached max strings to store\n");
				break;
			}
			total_string = 20;
			// Make sure you are giving the correct amount of memory.
			// Otherwise an error occurs, due to accessing more memory than was given.
			// *Cast Appropriately
			dyn_strings = (char **)realloc(dyn_strings, sizeof(char *) * total_string);
		}
		// copy buffer
		// -----------
		char *string = malloc(sizeof(char) * (str_len + 1));
		strcpy(string,buffer);

		// add string to table
		// -------------------
		dyn_strings[current_string] = string;
		printf("%s ", dyn_strings[current_string]);
		current_string++;
	}
	fclose(fptr);
}
