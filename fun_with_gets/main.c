#include <stdio.h>
#include <string.h>

int main() 
{

	puts("This is testing out the get functions!");
	puts("'gets' function");

	/* Buffer overflow: if input is greater than designated memory space */
	/* GCC has protection on this */

	char str[10];
	gets(str);

	printf("%s\n", str);

	puts("Enter for 'fgets'");
	char str_v1[10];
	fgets(str_v1, 10, stdin);

	int i;
	/* Show all chars in the array */
	for(i = 0; i < 10; i++) {
		printf("%c\n", str_v1[i]);
	}

	printf("%d\n", strlen(str_v1));
	printf("%s", str_v1);

	return 0;
}
