#include <stdio.h>

int main()
{

	int length = 12;
	int A[12] = {4,1,7,9,2,3,1,8,9,12,6,2};

	for (int i = 0; i < length; i++) {
		for (int j = length-1; j >= i; j--) {
			if (A[j] < A[j-1]) {
				int tmp = A[j-1];
				A[j-1] = A[j];
				A[j] = tmp;
			}	
		}	
	}

	// print sorted array
	for (int i = 0; i < length; i++) {
		printf("%d ", A[i]);
	}

}
