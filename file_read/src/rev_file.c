#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void rev_file(char *file_path);
char *append_strings(char *str1, char *str2, int total_length);

int main()
{
	rev_file("hello.txt");
}

void rev_file(char *file_path) {
	printf("%s\n", file_path);
	// file to read from
	// -----------------
	FILE *rfptr;
	rfptr = fopen(file_path, "r");

	if (rfptr == NULL) {
		printf("Unable to open read file");
		exit(1);
	}

	// append rev to file_path
	// -----------------------
	int file_path_length = strlen(file_path);
	int append_length = 4;

	int total_path = file_path_length + append_length;

	char *appended_str = append_strings("rev_", file_path, total_path);

	printf("%s\n", appended_str);


	// file to write to
	// ----------------
	FILE *wfptr;
	wfptr = fopen(appended_str, "w");

	long int beg_loc = ftell(rfptr);

	fseek(rfptr, 0, SEEK_END);
	char cur_char;
	while (ftell(rfptr) != beg_loc) {

		
		cur_char = fgetc(rfptr);
		//fscanf(rfptr, "%c", &cur_char);
		printf("%c", cur_char);

		fseek(rfptr, -2, SEEK_CUR);
	}
	cur_char = fgetc(rfptr);	// get last char
	printf("%c\n", cur_char);
	
}

char *append_strings(char *str1, char *str2, int total_length) {
	char *new_str = malloc(sizeof(char) * (total_length + 1));	// account for '\0' char

	if (new_str == NULL) {
		return NULL;
	}

	int length_str1 = strlen(str1);
	int length_str2 = strlen(str2);

	if ( (length_str1 + length_str2) != total_length) {
		printf("[Error] both strings not expected length\n");
		exit(1);
		return NULL;
	}
	
	int new_str_index = 0;
	for(int i = new_str_index; i < length_str1; i++, new_str_index++) {
		new_str[new_str_index] = str1[i];
	}

	for (int i = 0; i < length_str2; i++, new_str_index++) {
		new_str[new_str_index] = str2[i];
	}

	new_str[total_length] = '\0';

	return new_str;
		
}
