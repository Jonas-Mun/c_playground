#include <stdio.h>
#include <stdlib.h>

void freq_chars(int *freq);

int main()
{
	int freq_char[26] = {0};

	freq_chars(freq_char);

	for (int i = 0; i < 26; i++) {
		char curr_char = 'a' + i;
		printf("%c - %d\n", curr_char, freq_char[i]);
	}
}


void freq_chars(int *freq) {
	FILE *fptr;

	fptr = fopen("text.txt", "r");

	if (fptr == NULL) {
		printf("Unable to open file\n");
		exit(1);
	}

	char curr_char;
	int total_chars = 0;
	int index_char;
	while (fscanf(fptr, "%c", &curr_char) != EOF) {
		
		if (curr_char >= 'a' && curr_char <= 'z') {
			index_char = (int)(curr_char - 'a')	;
			freq[index_char]++;
		}

		total_chars++;
	}

	printf("Total chars read: %d\n", total_chars);
	fclose(fptr);
}
