#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>

/**
 * GETTING stuck:
 * Calculate size of pointer instead of allocated memory (Confused array with pointer)
 *
 *
 */

char *append_strings(int total_length, int num, ...) {

	va_list valist;

	va_start(valist, num);

	printf("[Given length} %d\n", total_length);
	char *new_str = malloc(sizeof(char) * total_length + 1);	// account for '\0' char
	int str_index = 0;

	if (new_str == NULL) {
		printf("Could not allocate memory\n");
		exit(1);
	}

	// append strings
	// --------------
	for(int i = 0; i < num; i++) {
		char *str = va_arg(valist, char *);
		int length = strlen(str);
		// copy chars
		// ----------
		for(int x = 0; x < length; x++, str_index++) {
			new_str[str_index] = str[x];
			printf("[char] %c\n", str[x]);
		}
	}

	new_str[total_length] = '\0';

	va_end(valist);

	return new_str;
}

int main() {
	char *a_str = append_strings(9 ,2, "hhuo ", "dude");
	printf("[length of str] length of string = %d\n", strlen(a_str));
	printf("%s\n", a_str);

}
