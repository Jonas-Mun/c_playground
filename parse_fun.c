#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

int compfunc(const void *a, const void *b) {
	const int arg1 = *((int *) a);
	const int arg2 = *((int *) b);

	if (arg1 > arg2) {
		return 1;
	}
	if (arg1 < arg2) {
		return -1;
	}
	return 0;
}

int main()
{
	char str[20] = "-1 2 -2 3";
	char delim = ' ';
	char *token;
	int sqr_vals[100] = {0};

	int val;
	int index = 0;
	int sqrd_val;
	token = strtok(str, &delim);
	while (token != NULL) {
		val = atoi(token);
		sqrd_val = pow(val, 2);
		sqr_vals[index] = sqrd_val;

		// update to next
		index++;
	token = strtok(NULL, &delim);	
	}
	qsort(sqr_vals, index, sizeof(int),compfunc);
	for(int i = 0; i < index; i++) {
		printf("%d ", sqr_vals[i]);
	}


	return 0;
}
