#include <stdio.h>
#include <stdlib.h>


int compfunc(const void *a, const void *b) {
	const int arg1 = *((int *) a);
	const int arg2 = *((int *) b);

	if (arg1 > arg2) {
		return -1;
	}
	if (arg1 < arg2) {
		return 1;
	}
	return 0;
}

int main()
{
	int A[24] = {5,1,3,9,3,6,31,11,5,85,3,44,12,43,23,34,8,1,89,5,23,11,2,3};


	qsort(A, 24, sizeof(int), compfunc);
	for(int i = 0; i < 24; i++) {
		printf("%d ", A[i]);
	}
	printf("\n");

}
