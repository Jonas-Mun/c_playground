#include <stdio.h>
#include <string.h>
#include <math.h>

int my_hash(char *str);

int main(int argc, char *argv[])
{

	for(int count = 1; count < argc; count++) {
		printf("%d\n", my_hash(argv[count]));
	}
}

int my_hash(char *str) {
	int n = strlen(str);
	int hash = 0;
	for(int i = 0; i < n; i++) {
		hash += pow(2,i) * str[i];	
	}
	return hash;
}
