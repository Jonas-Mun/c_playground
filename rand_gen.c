#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/** Random number generator
 * 	Code from response by: Jerry Coffin - StackOverflow
 *
 * 	Edited by: Jonas Munoz
 */

int rand_lim(int limit) {
	
	int divisor = RAND_MAX/(limit+1); /* Split range into 'limit' 
										 sections of divisor, accounting
										 for '0' (limit+1) */
	int retval;

	/* Discard remainder */
	do {
		retval = rand() / divisor;
	} while (retval > limit);

	return retval;
}

int main(void) {
	srand(time(0));	
	int limit = 10;
	for(int i = 0; i < limit; i++) {
		printf("%d\n", rand_lim(10));
	}


	return 0;
}
	
