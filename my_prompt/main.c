#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int main()
{
	puts("Enter a sentence");
	char* buffer;
	size_t length = 0;
	
	size_t bytes_read = getline(&buffer, &length, stdin);
	if (bytes_read <= 0) {
		printf("Error");
	}
	
	free(buffer);
}
