#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
/*
 * WORK IN PROGRESS
 */

/* returns the last position of space */
int acquire_word(char * str, int posititon, int size) {
	for (int i = 0; i < size; i++) {
		if (str[i] == ' ') {
			return i;
		}
	}
	// Last char of string
	return (size - 1);
}

void acquire_words(char ** dyn_buff, char * str, int position, int size) {

}

/*
 * Registers multiple spaces as a word
 */
int num_words(char * string, int size) {
	bool char_found = false;	// avoid ' '' ' scenarios

	// transverse string
	int word_count = 0;
	for (int i = 0; i < size; i++) {
		if (string[i] != ' ' && !char_found) {
			char_found = true;
		}
		if (string[i] == ' ' && char_found) {
			++word_count;
			char_found = false;
		}	
	}
	// Check last char is not continous space
	if (char_found) {
		++word_count;
		return word_count;
	}
	return word_count;
}

int main()
{
	/* getline used - includes the '\n' chracters*/
	/*
	char * buffer;
	size_t bufsize = 32;
	buffer = malloc(sizeof(char) * bufsize);
	printf("Enter a line of test\n");

	size_t characters = getline(&buffer,& bufsize, stdin);
	printf("%zu characters were read.\n", characters);
	printf("You typed: '%s'\n", buffer);


	free(buffer);
	*/

	/* fgets used */
	char str[60];

	printf("Using fgets, enter input\n");
	fgets(str, 60, stdin);
	//int pos = acquire_word(str, 0, 60);
	int word_num = num_words(str, 60);

	/* Dynamic Array of strings 
	 *
	 * P->s1
	 * P->s2
	 * ...
	 * P->sN
	 * N = number of words
	 */

	// Start utilizing pointers
	char ** wordsp;
	wordsp = malloc(sizeof(char *) * word_num);

	if (wordsp == NULL) {
		printf("Error");
		exit(1);
	}

	int word_end = acquire_word(str, 0, strlen(str));
	printf("%d\n", word_end);

	*wordsp = malloc(sizeof(char) * (word_end + 1));	//+1 -> '\0' char

	/* Copy word to dynamic pointer storage */
	for(int i = 0; i < (word_end); i++) {
		(*wordsp)[i] = str[i];
	}

	printf("%s", *wordsp);
	

	free(*wordsp);
	free(wordsp);


}
