#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argw)
{
	printf("Number of arguments: %d\n", argc);
	printf("%s\n",argw[2]);

	char c;
	printf("Enter a word:\n");
	c = getc(stdin);
	printf("First character: %c\n", c);

	exit(0);
}
