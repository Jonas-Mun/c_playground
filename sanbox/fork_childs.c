#include <stdio.h>

#include <sys/wait.h>

#include <unistd.h>

int main() {

		for (int i=0; i<3; i++) {

		pid_t child = fork();

		if (child > 0) {

		printf("Child %d created\n", child);

		wait(NULL);

		printf("Child %d terminated\n", child);

		}

		}

		printf("Parent terminated\n");

		return 0;

}
