#include <stdio.h>
#include <stdlib.h>

#define START_TOTAL_SIZE 128

void add_element(char c, char *buf, int current_element);

int main()
{
	int total_size = START_TOTAL_SIZE;
	char *dynamic = malloc(total_size);

	int go = 0;
	while (go < 2) {
		// fill array
		for (int i = 0; i < total_size; i++) {
			add_element('z', dynamic, i);
		}
		// Realloc
		total_size *= 2;
		printf("total size: %d\n", total_size);
		dynamic = (char *) realloc(dynamic, total_size);
		go = go + 1;
	}
	free(dynamic);
}

void add_element(char c, char *buf, int current_element) {
	buf[current_element] = c;
}
