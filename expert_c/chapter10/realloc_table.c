#include <stdio.h>
#include <stdlib.h>

#define MAX_ELEMENT 128

int current_element = 0;
int total_element = MAX_ELEMENT;
char *dynamic; 

dynamic = (char *)malloc(MAX_ELEMENT);

void add_element(char c) {

	if (current_element == total_element-1) {
		total_element *= 2;
		dynamic = (char *) realloc(dynamic, total_element);
		if (dynamic == NULL) {
			printf("ERROR: Could not expand table");
		}
	}
	current_element++;
	dynamic[current_element] = c;

}

int main() 
{
	add_element('x');
}
