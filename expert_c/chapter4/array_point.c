#include <stdio.h>
#include <stdlib.h>

int main() {
	int *p = malloc(sizeof(int));	
	int arr[100];

	printf("Address in pointer: %d\n", p);
	printf("Address to pointer: %d\n", &p);

	printf("Address of array: %d\n", arr);

	free(p);
}
