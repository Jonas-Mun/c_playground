#include <stdio.h>

class Fruit {
	public: virtual void peel();
		void slice(int num);
		float juice();
	private: int weight;
		 int calories_per_oz;
};

Fruit::Fruit(int i, int j) {
	weight = i;
	calories_per_oz = j;
}

void Fruit::peel() {
	printf("in peel");
}

void Fruit::slice(int num) {
	printf("In slice %d", this->weight);
}

float Fruit::juice() {
	printf("in juice");
	return 12;
}

class Apple : public Fruit {
	public:
		void peel() {
			printf("peeling an apple\n");
		}
		void make_candy_apple(float weight);
}


Fruit melon(4,5), orange(1,2), banana(0,0);
Fruit * p;
p = new Apple;
p->peel();

int main() {
	melon.slice(2);
	orange.juice();
	return 0;
}
