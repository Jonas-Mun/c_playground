#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#define MAXTOKENLEN 256
#define MAXTOKENS 50

#define NUMQUALIFIERS 2
#define NUMTYPES 5

#define SAVEMEMORY 0

char **arguments;
int max_index = 0;
int arg_index = 1;

/* structure */
struct token {
		char type;
		char string[MAXTOKENLEN];
};

struct token stack[MAXTOKENS];
struct token this;

enum token_tag { TYPE, QUALIFIER, IDENTIFIER};


/* Functions */

#if SAVEMEMORY
enum token_tag classify_string(struct token current) {
	if (strcmp(current.string, "const") == 0) {
		return QUALIFIER;
	} 
	if (strcmp(current.string, "volatile") == 0) {
		return QUALIFIER;
	}
	if (strcmp(current.string, "int") == 0) {
		return TYPE;
	}
	if (strcmp(current.string, "char") == 0) {
		return TYPE;
	}
	if (strcmp(current.string, "float") == 0) {
		return TYPE;
	}
	if (strcmp(current.string, "double") == 0) {
		return TYPE;
	}
	if (strcmp(current.string, "struct") == 0 ) {
		return TYPE;
	}
	return IDENTIFIER;
}
#else
char *qualifiers[] = {"const", "volatile"};
char *types[] = {"int", "char", "float", "double", "struct"};

enum token_tag classify_string(struct token current) {
	
	// Qualifiers
	for (int i = 0; i < NUMQUALIFIERS; i++) {
		if (strcmp(current.string, qualifiers[i]) == 0) {
			return QUALIFIER;
		}
	}

	// Types
	for (int i = 0; i < NUMTYPES; i++) {
		if (strcmp(current.string, types[i]) == 0) {
			return TYPE;
		}
	}
	return IDENTIFIER;
}
#endif

int gettoken() {

	if ((arg_index) >= max_index) {
		printf("Reached limit of arguments");
		return -1;
	}
	char  word[MAXTOKENLEN]; 
	strcpy(word, arguments[arg_index]);
	strcpy(this.string, word);

	if (isalnum(this.string[0])) {
		this.type = classify_string(this);
	} else {
		this.type = this.string[0];
		this.string[0] = '\0';
	}

	arg_index = arg_index + 1;
	return 0;
}

/*
 *@stack: global stack to push tokens in
 *
 *
 */
void read_to_first_identifier(struct token * stack) {
	int stack_index = 0;
	gettoken();

	printf("%c", this.type);

	while(this.type != IDENTIFIER) {
		// push new token
		struct token newToken;
		newToken.type = this.type;
		strcpy(newToken.string, this.string);

		stack[stack_index] = newToken;

		stack_index += 1;
		gettoken();
	}
	printf("Identifier is: %s\n", this.string);
	gettoken();
}

void deal_with_function_args(char ** commands) {
	while(commands[arg_index][0] != ')') {
		arg_index++;
	}
	printf("function returning");
}

void deal_with_arrays(char ** commands) {
	bool on_array = true;	// State of array
	bool end_of_array = false;

	
	
	while(on_array) {
		if (arg_index > max_index) {
			printf("Index out of bounds: %c",this.type);

			on_array = false;
			break;
		}

		if (this.type == '[') {
			printf("%c", this.type);
			end_of_array = false;

			gettoken();
		} else if (this.type == ']') {
			printf("%c", this.type);

			end_of_array = true;

			if (gettoken() == -1) {
				on_array = false;		
			}
		} else if (end_of_array && (this.type != '[')) {
			on_array = false;
			printf("\nArray finished\n");
		} else {
			printf(" size ");
			gettoken();
		}
	}
	
}

void deal_with_any_pointers(char ** commands) {
	while(commands[arg_index][0] == '*') {
		printf("Pointer to");
		//Pop it
	}
}

void deal_with_declarator() {
	if (this.type == '[') {
		printf("Array\n");
		deal_with_arrays(arguments);
	} 
	if (this.type == '(') {
		printf("Functions\n");
		deal_with_function_args(arguments);
	} else {
		//deal_with_any_pointers(arguments);
		printf("%c", this.type);
	}

	/*
	while(stack != NULL) {
		if (this.string[0] == '(') {
			//Pop it
			gettoken();
			deal_with_declarator();
		} else {
			//Pop it
			printf("%s", this.string);
		}
	}*/
}

int main(int argc, char * argv[]) 
{
	// Pass arguments
	arguments = argv;
	max_index = argc;

	for (int i = 0; i < max_index; i++) {
		printf("%s\n", argv[i]);
	}
	

	printf("Max Index: %d\n", max_index);

	read_to_first_identifier(stack);
	//printf("%c", this.type);
	deal_with_declarator();

}
