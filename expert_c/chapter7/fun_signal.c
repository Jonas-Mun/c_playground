#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

void hello(int signum) {
	printf("CATCHED!\n");
	exit(0);
}

int main()
{
	raise(SIGINT);
	signal(SIGINT, hello);
	while(1) {
		printf("Hello");
	}

}
