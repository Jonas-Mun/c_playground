#include <stdio.h>
#include <signal.h>
#include <setjmp.h>

jmp_buf buf;

void handler(int i) {
	printf("restarting program\n");
	char r[1];
	puts("Response (Y/n)\n");
	scanf("%c", r);
	longjmp(buf,1);

}

int main()
{
	signal(SIGINT, handler);

	if (setjmp(buf)) {
		printf("Changed my mind... Goodbye");
		return 0;
	}
	while(1) {

	}
}
