#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

void seg_fault() {
	printf("Segmentation fault (Accessing unwarrented memory location)!!\n");
	exit(1);
}

int main()
{
	signal(SIGSEGV,seg_fault);
	int *p= 0;
	*p = 17;
}
