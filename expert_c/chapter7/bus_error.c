#include <stdio.h>

/**
 * According to Stack Overflow, bus errors are are
 * in x86 architecture
 *
 * Won't make an error, but can leave a nasty trail.
 */
int main()
{
	union { char a[10];
			int i;
	} u;

	int *p = (int*) &(u.a[1]);
	*p = 17;
	printf("%c", *p);
}
