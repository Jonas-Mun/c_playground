#include <stdio.h>
#include <setjmp.h>

jmp_buf buf;

void a (int i) {
	if (i > 0) {
		a(--i);
	} else {
		printf("i has reached zero\n");
		longjmp(buf,1);
	}
}

int main () {
	if (setjmp(buf)) {
		printf("Came back form a\n");
		return 0;
	} 

	a(1);
	return 0;
}
