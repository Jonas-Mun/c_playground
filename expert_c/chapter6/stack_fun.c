#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void dummy_func() {
	int dummy_arr[10000];
	printf("Dummy array at: %p\n", &dummy_arr);

	int top;
	printf("Top of stack: %p\n", &top);
}

int main() 
{
	int i;
	int j;
	int o;
	printf("The stack top is near %p, %p, %p\n", &i, &j, &o);	

	int num = 2;
	int num_two = 4;

	printf("The stack top is near: %p, %p\n", &num, &num_two);

	int *p = malloc(sizeof(int) * 4);

	printf("The pointer is at: %p\n", &p);

	int arr[10];

	printf("The array begins at: %p\n", &arr);

	dummy_func();

	int top;

	printf("top stack: %p\n", &top);


	free(p);
	return 0;
}
