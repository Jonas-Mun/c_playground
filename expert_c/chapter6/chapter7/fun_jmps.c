#include <stdio.h>
#include <setjmp.h>

jmp_buf buf;

void banana() {
	printf("In banana()\n");
	longjmp(buf, 2);
	/*NOTREACHED*/
	printf("You'll never see this, because I longjmp'd");
}

int main()
{
	if (setjmp(buf) == 2) {
		printf("back in main\n");
	} else {
		printf("first time through\n");
		banana();
	}
}

