/**
 * OBSELETE method for declaring prototypes.
 * Exists for compatibility reasons.
 */

#include <stdio.h>

int foo();


int main()

{
	printf("Testing out K&R prototypes\n");
	foo(2,3);
}

int foo(a, b)
	int a;
	int b;
{
	printf("In foo\n");
}
