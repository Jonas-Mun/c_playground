#include <stdio.h>

int main()
{
	int c;

	system("stty raw");

	while(1) {

		c = getchar();
		if (c == 'e') {
			break;
		}
		printf("%c", c);
	}

	system("stty cooked");
}
