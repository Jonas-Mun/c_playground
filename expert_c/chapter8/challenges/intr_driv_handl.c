#include <stdio.h>
#include <signal.h>
#include <sys/stropts.h>
#include <sys/ioctl.h>

int iteration = 0;

int signal_handler(int i) {
	char c = ' ';
	c = getchar();

	if (c == 'q') {
		return 1;
	} else {
		printf("Got char: %c\n", c);
		return 0;
	}

}

int main()
{
	sigset(SIGPOLL, signal_handler);
	ioctl(0, I_SETSIG, S_RDNORM);

	for(;;iteration++);

}
