/**
 * specifying the type in the protoype prevents type promotion from occuring
 */

#include <stdio.h>

int foo(int a, int b);

int main(void)
{
	printf("Testing out ANSI prototypes\n");
	foo(3,2);
}

int foo(int a, int b) {
	printf("In foo\n");
}
