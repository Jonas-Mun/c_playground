*Type promotion
  - usually promoted to int if type is unknown.
  - Passing function arguments can be promoted, if prototype is not defined.
* Why type conversion?
  - wanted to simplify the compilers. (Kludge)
  - Parameters on stack would be same length, able to know the many arguments.

* prototypes
* blocking I/O
* polling I/O
* Hash
* raw input
* cooked input

Mostly abaout how C handles data, specifically how to handle various inputs.
