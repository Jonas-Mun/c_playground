#include <stdio.h>

void array_param(int numbers[]) {
	printf("%p, %p, %p\n", &numbers, &(numbers[0]), &(numbers[1]));
}

void pointer_param(int *pa) {
	printf("%p, %p, %p, %p\n", &pa, &(pa[0]), &(pa[1]), ++pa);
}

int ga[] = {'a','b','c','d','e','f','g','h','i','j','k','l','m','o','p','q','r','s','t','u','v','w','x','y','z'};

/**
 * I expect:
 * array_param: 1- address to poiner, 2- 1, address to element 1
 * pointer_param: 1- address to pointer, 2- 1, 3- address to element 4 - 3
 *
 */

int main()
{
	
	array_param(ga);
	pointer_param(ga);

	printf("Main Function: %p, %p, %p", &ga, &(ga[0]), &(ga[1]));
}
