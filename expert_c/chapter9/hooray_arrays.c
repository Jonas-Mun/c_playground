#include <stdio.h>

int main()
{
	int apricot[2][3][5];

	int (*r)[5] = apricot[0];
	int *t = apricot[0][0];

	printf("%x, %x\n",r[0], t);

	r++;
	t++;
	printf("%x, %x\n",r[0], t);
}
