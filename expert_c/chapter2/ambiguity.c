#include <stdio.h>

/*
 * Check ambiguity whether multiply by a value or dereferencing a pointer
 */

int main()
{
	size_t apple;

	int p = 4;

	int *pp = malloc(sizeof(int));
	*pp = p;

	apple = sizeof (int) * *pp; // Ambigous


	printf("%d", apple);
}
