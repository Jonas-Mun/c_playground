#include <stdio.h>

/*
 * Sins of mission:
 *
 * Token ambiguity: Keywords have different meanings depending
 * on the context they are used.
 *
 * static funcion() -> local scope function
 * function() {
 * 		static int i = 2;
 * 	}
 *
 * 		-> retains value between calls
 *
 * Operators have wrong precedence
 */

int main() 
{
	int i;
	i=1,2;
	//(i=1), 2; /* i gets the value 1 */
	printf("%d", i);
}
