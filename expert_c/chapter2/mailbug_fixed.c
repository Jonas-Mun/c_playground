#include <stdio.h>
/*
 * Bug faciliated by the inadequate classification of arguments.
 * Where do options end, and non-option arguments begin.
 */

int main(int argc, char *argv[])
{
	if (argv[argc-1][0] == '-' ||
		argv[argc-2][0] == '-' && (argv[argc-2][1] == 'f')) {
		printf("Read mail");
	} else {
		printf("Send mail");
	}
}
