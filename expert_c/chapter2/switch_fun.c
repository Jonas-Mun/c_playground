#include <stdio.h>
/*
 * 'break;' can be ambigous in terms of where it breaks
 * What caused the AT&T incident.
 */

int main()
{
	int i = 2;
	const int two = 0;

	switch(i) {
		case 1: printf("case 1 \n");
		//case two: printf("case 2 \n"); Error
		case 2: printf("case 2 \n");
	}

	/* According to Expert C Programming, Default
	 * FALLTHRU is 97% wrong
	 */
	switch(i) {
		case 1 :printf("case 1 \n");
				/* FALLTHRU */
		case 2 : printf("case 2 \n");
		case 3 : printf("case 3 \n");
	}

	switch(i) {
		case 1: printf("case 1");
				break;
		case 2: printf("case 2");
				break;
		case 3: printf("case 3");
				break;
		case 4: printf("case 4");
				break;
		default: printf("Nothing");
	}
}
