#include <stdio.h>

/*
 * Bug that prevented sending email to users with 'f' as a second character
 *
 * mail effie addressl
 * -> Readmail
 */

int main(int argc, char *argv[])
{
	//printf("%d %s\n", argc, argv[0]);

	if (argv[argc-1][0] == '-' || (argv[argc-2][1] == 'f') ){
		printf("Readmail: The bug");
	} else {
		printf("Sendmail");
	}
}
