#include <stdio.h>

/**
 * Converts 'int' variable to 'char'
 *
 * '0' character has ASCII value of 48.
 * '9' character has ASCII value of 57.
 *
 *  5 + '0' == 5 + 48
 *  Giving the int equivalent of 5 in ASCII char: 53.
 */

int main (void)
{
	int num = 5;
	char alph = num + '0'; // Convert 'int num' to 'char'
	int beta = 6 + '0';

	printf("%c",beta);	// C formats output based on specifier (Ignores given type)
	printf("%d", beta);	
}
