#include <stdio.h>
#include <stdlib.h>

void square(int radius);

int main(int argc, char *argv[]) 
{
	if (argc < 2) {
		printf("[!!] Not enough arguments\n");
		exit(1);
	}

	int radius = atoi(argv[1]);
	square(radius);

	return 0;
}

void square(int radius) {
	int distance = radius + radius;

	// first row
	// --------
	for (int i = 0; i < distance; i++) {
		printf("#");
	}
	printf("\n");

	int num_middle_rows = distance - 2;	// exclude first and last row
	// middle row
	// ----------
	for (int row = 0; row < num_middle_rows; row++) {
		for (int i = 0; i < distance; i++) {
			if (i == 0 || i == distance - 1) {
				printf("#");
			} else {
				printf(" ");
			}
		}
		printf("\n");
	}

	// last row
	// --------
	for (int i = 0; i < distance; i++) {
		printf("#");
	}
	printf("\n");
}
