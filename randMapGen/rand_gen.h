#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int rand_lim(int limit) {
	
	int divisor = RAND_MAX / (limit+1); /* Splits 'l' sections into range of 'n'
										 * where 'n' is the divisor 
										 * and 'l' is limit
										 */
	int retval;

	do {
		retval = rand() / divisor;
	} while(retval > limit);

	return retval;
}
