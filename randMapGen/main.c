#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "rand_gen.h"

enum Terrain {water, land, mountain};

int main(void)
{
	srand(time(0));
	int x = 10;
	int y = 10;
	int ter_map[x][y];

	/* Fill terrain map */
	for (int i = 0; i < x; i++) {
		for (int j = 0; j < y; j++) {
			ter_map[i][j] = rand_lim(2);
		}
	}

	/* print terrain map */
	for (int j = 0; j < y; j++) {
		for (int i = 0; i < x; i++) {
			printf("%d ", ter_map[i][j]);
		}
		printf("\n");
	}

	return 0;
}
